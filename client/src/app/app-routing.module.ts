import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';
import { RegisterComponent } from './components/register/register.component';
import { EventsComponent } from './components/events/events.component';
import { UserEventsComponent } from './components/events/user-events/user-events.component';
import { SingleEventComponent } from './components/events/single-event/single-event.component';
import { NewEventComponent }from './components/new-event/new-event.component';
import { LoginComponent } from './components/auth/login/login.component';
import { AuthGuard } from './authGuard';
const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {path:'login',component: LoginComponent},
  {path:'profile',component: ProfileComponent,canActivate: [AuthGuard]},
  {path:'register',component: RegisterComponent},
  {path:'events/:id',component: SingleEventComponent,canActivate: [AuthGuard]},
  {path:'user-events',component: UserEventsComponent,canActivate: [AuthGuard]},
  {path:'new_event',component: NewEventComponent,canActivate: [AuthGuard]},
  {path:'events',component: EventsComponent,canActivate: [AuthGuard]},
  {path: '**', redirectTo: 'login'}
]
/**
];
{path: '', pathMatch: 'full', redirectTo: 'login', canActivate:[LoggedInAuthGuard]},
  {path:'login',component: LoginComponent, canActivate:[LoggedInAuthGuard]},
  {path:'profile',component: ProfileComponent,canActivate: [AuthGuard]},
  {path:'register',component: RegisterComponent, canActivate:[LoggedInAuthGuard]},
  {path:'events/:id',component: SingleEventComponent,canActivate: [AuthGuard]},
  {path:'user-events',component: UserEventsComponent,canActivate: [AuthGuard]},
  {path:'new_event',component: NewEventComponent,canActivate: [AuthGuard]},
  {path:'events',component: EventsComponent,canActivate: [AuthGuard]},
  {path: '**', redirectTo: 'login', canActivate:[LoggedInAuthGuard]}
 */
@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
