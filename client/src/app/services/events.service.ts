import { Injectable } from '@angular/core';
import {EventData} from '../classes/event-data';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EventsService {

  events: EventData[]=[];

  private readonly url = '/api/events';

  public eventsSubject: BehaviorSubject<EventData[]> = new BehaviorSubject<EventData[]>(null);

  constructor(private readonly http: HttpClient) {
    this.getAllEvents().subscribe();
  
   }
   getById(id: string): Observable<EventData>{
    return this.http.get<EventData>(`${this.url}/${id}`);
  }

  postEvent(event: EventData,image:File): Observable<EventData> {
    const eventData = new FormData();
    eventData.append("name", event.name);
    eventData.append("description", event.description);
    eventData.append("place", event.place);
    eventData.append("adress", event.adress);
    eventData.append("time", event.time.toString());
    eventData.append("image", image, event.name);
    eventData.append("category",event.category);
    eventData.append("createdBy",event.createdBy);
    const headers: HttpHeaders = new HttpHeaders({
      authorization: 'Bearer ' + sessionStorage.token,
    });
    return this.http
      .post<EventData>(this.url, eventData, { headers })
      .pipe(
        tap((eventData :EventData) =>{
        console.log(eventData);
          this.eventsSubject.next([...this.eventsSubject.getValue(), eventData]) ;
        }
        )
      );
  }
  
  getAllEvents(): Observable<EventData[]>{
    return this.http.get<EventData[]>(this.url)
      .pipe(
        tap((events: EventData[]) => this.eventsSubject.next(events))
      );
  }
  delete(id: string): Observable<void> {
    const headers: HttpHeaders = new HttpHeaders({
      authorization: 'Bearer ' + sessionStorage.token,
    });
    return this.http
      .delete<void>(`${this.url}/${id}`, { headers })
      .pipe(
        tap(() =>
          this.eventsSubject.next(
            this.eventsSubject
              .getValue()
              .filter((event: EventData) => event.id !== id)
          )
        )
      );
  }
}
