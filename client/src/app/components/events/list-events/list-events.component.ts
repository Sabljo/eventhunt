import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { EventData } from '../../../classes/event-data';
import { BehaviorSubject, Subscription,Observable } from 'rxjs';
import {EventsService} from '../../../services/events.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-list-events',
  templateUrl: './list-events.component.html',
  styleUrls: ['./list-events.component.css']
})
export class ListEventsComponent implements OnInit {

  events : EventData[];
  show : boolean;
  forms: FormGroup;
  filteredEvents: Observable<EventData[]>;
  CountryData: Array<any> = [
    { name: 'Social event', value: 'Social event' },
    { name: 'Sport', value: 'Sport' },
    { name: 'Festivals', value: 'Festivals' },
    { name: 'Fundraising event', value: 'Fundraising event' },
    { name: 'Education', value: 'Education' }
  ];
  SortData :Array<any>=[
    { name: 'Oldest to newest', value: 'inc' },
    { name: 'Newest to oldest', value: 'desc' },
    { name: 'None', value: 'no' }
  ];
  eventsSubject: BehaviorSubject<EventData[]>;
  constructor(private readonly eventsService: EventsService,private readonly _activatedRoute:ActivatedRoute,private fb: FormBuilder) { 
    this.forms = this.fb.group({
      checkArray: this.fb.array([]),
      checkArray2: this.fb.array([]),
      Location: new FormControl(null),
      Date1: new FormControl(null),
      Date2: new FormControl(null)
    })
  }

  ngOnInit(): void {
    this.eventsSubject = this.eventsService.eventsSubject;
    this.eventsService.eventsSubject.subscribe();
    if(this._activatedRoute.snapshot.routeConfig.path=='events'){
      this.show=true;
    }else{
      this.show=false;
    }
    this.filteredEvents = this.eventsSubject.pipe();
  }
  delete(id: string): void{
    this.eventsService.delete(id);
  }

  onDeleteClick(event: EventData){
    this.eventsService.delete(event.id).subscribe();
    console.log(this.events);
  }

  submitForm(){
    console.log(this.forms.value);
    this.filteredEvents = this.eventsSubject.pipe(
                map(events => events.filter(event => 
                  this.filtersFunc(event)
                  ) ));
    if(this.forms.value.checkArray2[0]=="inc"){
      this.sortByDateAsc();
    }
    if(this.forms.value.checkArray2[0]=="desc"){
      this.sortByDateDesc();
    }
  }
  sortByDateDesc() {
    this.filteredEvents = this.filteredEvents.pipe(map((rallies => rallies.sort((a, b) => new Date(b.time).getTime() - new Date(a.time).getTime()))))
  }

  sortByDateAsc() {
    this.filteredEvents = this.filteredEvents.pipe(map((rallies => rallies.sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime()))))
    }
  filtersFunc(event:EventData){
    let c=true;
    if(this.forms.value.Location!=null && event.place!=this.forms.value.Location){
      c=c && false;
    }
    const checkArray3: FormArray = this.forms.get('checkArray') as FormArray;
    if(checkArray3.length>0){
    let i: number = 0;
    let b:boolean=false;
      checkArray3.controls.forEach((item: FormControl) => {
        if (item.value == event.category) {
            b=true;
        }
        i++;
      });
    c=c && b;
    }
    if(this.forms.value.Date1!=null && this.convert(event.time)<this.forms.value.Date1){
      console.log(event.time);
      c=c && false;
    }
    if(this.forms.value.Date2!=null && this.convert(event.time)>this.forms.value.Date2){
      console.log(event.time);
      c=c && false;

    }
    return c;
  }
  convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }
  onCheckboxChange(e) {
    const checkArray: FormArray = this.forms.get('checkArray') as FormArray;
  
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  onCheckboxChange2(e) {
    const checkArray2: FormArray = this.forms.get('checkArray2') as FormArray;
  
    if (e.target.checked) {
      if(checkArray2.length>0){
        checkArray2.removeAt(0);
      }
      checkArray2.push(new FormControl(e.target.value));
    } 
  }
  cancelForm(){
    this.forms = this.fb.group({
      checkArray: this.fb.array([]),
      checkArray2: this.fb.array([]),
      Location: new FormControl(null),
      Date1: new FormControl(null),
      Date2: new FormControl(null)
    })
    console.log(this.forms.value);
    this.filteredEvents = this.eventsSubject.pipe();
  }
  
}
