import { Component, Input, OnInit,EventEmitter,Output } from '@angular/core';
import { EventData } from '../../../classes/event-data';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EventsService } from '../../../services/events.service';
@Component({
  selector: 'app-single-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.css']
})
export class SingleEventComponent implements OnInit {

  private id: string;
  event: Observable<EventData>;

 
  constructor(
    private readonly eventsService: EventsService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) =>{
      this.id = String(params.id);
      this.event = this.eventsService.getById(this.id);
      console.log(this.id);
      console.log(this.event);
    });
  }

}
