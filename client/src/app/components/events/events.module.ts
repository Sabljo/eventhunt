import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from '../../app-routing.module';


import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



import { ListEventsComponent } from './/list-events/list-events.component';
import { EventComponent } from './event/event.component';
import { SingleEventComponent } from './single-event/single-event.component';
import { UserEventsComponent } from './user-events/user-events.component';
import { NewEventComponent } from '../new-event/new-event.component';
import { CommonModule } from '@angular/common';
import { EventsService } from '../../services/events.service';
import { EventsComponent } from './events.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListEventsComponent,
    EventComponent,
    SingleEventComponent,
    UserEventsComponent,
    NewEventComponent,
    EventsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    EventsComponent
  ],
  providers: [{provide: EventsService, useClass: EventsService}]
})
export class EventsModule { }
