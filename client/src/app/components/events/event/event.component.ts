import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';

import { EventData } from '../../../classes/event-data';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  @Input() event : EventData;
  @Input() show : boolean;

  @Output()
  deleteClick: EventEmitter<EventData> = new EventEmitter<EventData>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.show);
  }
  onDeleteClick(event: MouseEvent){
    this.deleteClick.emit(this.event);
    event.preventDefault();
    event.stopPropagation();
    console.log("tuu");
  }


}
