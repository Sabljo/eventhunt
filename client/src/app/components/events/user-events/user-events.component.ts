import { Component, OnInit } from '@angular/core';
import { EventData } from '../../../classes/event-data';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import {EventsService} from '../../../services/events.service';
import { Router } from '@angular/router';
import { AuthService } from '../..//auth/auth.service';
import { User } from '../../../classes/user';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-user-events',
  templateUrl: './user-events.component.html',
  styleUrls: ['./user-events.component.css']
})
export class UserEventsComponent implements OnInit {

  events : EventData[];
  private eventsSubscription: Subscription;
  show : boolean;
  currentUser: User;
  eventsSubject: BehaviorSubject<EventData[]>;
  filteredEvents: Observable<EventData[]>;
  constructor(private readonly eventsService: EventsService,
    private readonly router:Router,
    private readonly authService: AuthService
    ) { }

  ngOnInit(): void {
    this.eventsSubject = this.eventsService.eventsSubject;
    this.eventsService.eventsSubject.subscribe((events: EventData[]) => console.log(events));
    this.show=false;
    this.authService.currentUserSubject.subscribe((user)=>this.currentUser=user);
    this.filteredEvents = this.eventsSubject.pipe(
      map(events => events.filter(event => event.createdBy==this.currentUser.id ))
   );
  }
  clicked(event_id) {
    this.router.navigate(['/events',event_id]);
}
delete(id: string): void{
  this.eventsService.delete(id);
}

ngOnDestroy(): void{
  //this.quotesSubscription.unsubscribe();
}

onDeleteClick(event: EventData){
  this.eventsService.delete(event.id).subscribe();
  console.log(this.events);
}


}
