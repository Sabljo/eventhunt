import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { EventData } from '../../classes/event-data';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  showOne: boolean =false;
  eventToShow : EventData;
  constructor() { }

  ngOnInit(): void {
  }
  onEventClick( event: EventData){
    this.showOne=true;
    this.eventToShow=event;
  }
}
