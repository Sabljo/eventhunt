import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
    email: new FormControl()
  });

  constructor(private readonly authService: AuthService,
    private readonly router: Router) {}

  ngOnInit(): void {}

  register() {
    const { username, password, email } = this.registerForm.controls;
    this.authService.register(username.value, password.value, email.value).subscribe(()=>this.router.navigate(['./login']));
  }
}