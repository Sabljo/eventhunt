import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  });

  constructor(private readonly authService: AuthService,
    private readonly router: Router) {}

  ngOnInit(): void {
  }

  login() {
    const { username, password } = this.loginForm.controls;
    this.authService.login(username.value, password.value).subscribe(()=>this.router.navigate(['./events']));
  }
}
