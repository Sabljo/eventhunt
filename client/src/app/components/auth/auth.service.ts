import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Token } from './token.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private url: string = '/api/users/login';
  private url2: string = '/api/users/register';

  isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject(null);
  currentUserSubject = new BehaviorSubject(null); 

  constructor(private readonly http: HttpClient) {
    if (sessionStorage.token) {
      this.tokenSubject.next(sessionStorage.token);
      this.isLoggedIn.next(true);
    }
  }
  ngOnInit(){
  }
  login(username: string, password: string): Observable<any> {
    return this.http
      .post<any>(this.url, { username, password })
      .pipe(
        tap((token) => {
          sessionStorage.token = token.token;
          this.tokenSubject.next(token.token);
          this.currentUserSubject.next(token.user);
          this.isLoggedIn.next(true);
        })
      );
  }

  register(username: string, password: string, email:string): Observable<Token> {
    return this.http
      .post<Token>(this.url2, { username, password ,email})
      .pipe(
        tap((token) => {
          sessionStorage.token = token.token;
        })
      );
  }
  ngOnDestroy(): void{
    delete sessionStorage.token;
    this.tokenSubject.next(null);
    this.isLoggedIn.next(false);
    this.currentUserSubject.next(null);
  }
  logout() {
    delete sessionStorage.token;
    this.tokenSubject.next(null);
    this.isLoggedIn.next(false);
    this.currentUserSubject.next(null);
  }
}
