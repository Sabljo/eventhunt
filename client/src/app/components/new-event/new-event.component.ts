import { Component, OnInit } from '@angular/core';
import { EventData } from 'src/app/classes/event-data';
import { Router } from '@angular/router';
import { BehaviorSubject, Subscription, Observable } from 'rxjs';
import { Location } from '@angular/common';
import {EventsService} from '../../services/events.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup,FormControl } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { User } from '../../classes/user';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.component.html',
  styleUrls: ['./new-event.component.css']
})
export class NewEventComponent implements OnInit {

  form: FormGroup;
  imageData: string;
  currentUser: User;

  private createEventSubscription: Subscription;
  constructor(
    private readonly router: Router,
    public readonly eventsService: EventsService,
    private readonly location:Location,
    private readonly http : HttpClient,
    private readonly authService: AuthService,

    ) {
   }

   ngOnInit(): void {
    this.authService.currentUserSubject.subscribe((user)=>this.currentUser=user)
    this.form = new FormGroup({
      EventName: new FormControl(null),
      Location: new FormControl(null),
      Description: new FormControl(null),
      Time: new FormControl(null),
      Date: new FormControl(null),
      image: new FormControl(null),
      adress: new FormControl(null),
      category: new FormControl(null)
    });
  }
  onFileSelect(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    const allowedMimeTypes = ["image/png", "image/jpeg", "image/jpg"];
    if (file && allowedMimeTypes.includes(file.type)) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imageData = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  onSubmit() {
    const dateTime = this.form.value.Date+'T'+this.form.value.Time+':00';
      let newEvent: EventData={
        name: this.form.value.EventName,
        time: new Date(dateTime),
        place: this.form.value.Location,
        description : this.form.value.Description,
        picture : this.imageData,
        adress: this.form.value.adress,
        category: this.form.value.category,
        createdBy: this.currentUser.id
      }
      this.createEventSubscription=this.eventsService.postEvent(newEvent,this.form.value.image).subscribe();
      this.form.reset();
      this.imageData = null;
      this.router.navigate(['/events']);
      
    
  }


  

}
