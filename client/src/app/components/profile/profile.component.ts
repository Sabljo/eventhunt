import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {


  isLoggedIn: Observable<boolean>;
  currentUser: Observable<any>;
  constructor(private readonly authService: AuthService,
    private readonly router:Router) {
      this.isLoggedIn = this.authService.isLoggedIn;
      this.authService.currentUserSubject.subscribe((user)=>this.currentUser=user);
    }

  ngOnInit(): void {
    console.log(this.currentUser);
  }


}