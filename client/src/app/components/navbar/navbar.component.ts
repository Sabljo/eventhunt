import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: Observable<boolean>;
  currentUser: Observable<any>;
  constructor(private readonly authService: AuthService,
    private readonly router:Router) {
      this.isLoggedIn = this.authService.isLoggedIn;
      this.authService.currentUserSubject.subscribe((user)=>this.currentUser=user);
    }

  ngOnInit(): void {
   console.log("pocetak");
    console.log(this.isLoggedIn);
    console.log(this.currentUser);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['./login']);
  }

}
