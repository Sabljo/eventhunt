import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/profile/profile.component';

import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import {EventsModule} from './components/events/events.module';
import { AuthModule } from './components/auth/auth.module';
import { LoggedInAuthGuard } from './loggedInAuthGuard';
import { AuthGuard } from './authGuard';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegisterComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    EventsModule,
    ReactiveFormsModule,
    AuthModule
  ],
  providers: [AuthGuard,LoggedInAuthGuard],
  bootstrap: [AppComponent],
  exports:[FormsModule,
    ReactiveFormsModule]
})
export class AppModule { }
