export class EventData {
    name: string;
    time: Date;
    place: string;
    adress: string;
    description : string;
    picture : string;
    category: string;
    id?: string;
    createdBy: string;
}
