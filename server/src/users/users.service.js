
import { database } from '../database/database.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import config from 'config';

export class UsersService {
  static _collection = database.collection('users');

  static async register(user) {
    if (await UsersService._collection.findOne({ username: user.username })) {
      console.log("tuuu")
      throw { code: 409, message: 'Username taken!' };
    }
    try {
      user.password = await bcrypt.hash(user.password, 10);
      const users=await UsersService._collection.insertOne(user);
      const token = UsersService._signToken(users);
      console.log(users);
      return token;
    } catch (error) {
      throw { code: 500, message: 'Internal server error' };
    }
  }

  static async login(username, password) {
    try {
      const user = await UsersService._collection.findOne({ username });
      console.log(user);
      const user_log={name: user.username,email:user.email,id:user._id}

      if (!user || !(await bcrypt.compare(password, user.password))) {
        console.log("tuuuuu")
        throw { code: 401, message: 'Wrong credentials!' };
      }
      const token = UsersService._signToken(user);
      console.log("tuu")
      return {token:token,user:user_log};
    } catch (error) {
      if (error.code === 401) {
        throw { code: 401, message: 'Wrong credentials!' };
      }
      throw { code: 500, message: 'Internal server error' };
    }
  }

  static _signToken(user) {
    const jwtConfig = config.get('jwt');
    return jwt.sign({ id: user._id, username: user.username, email: user.email }, jwtConfig.secret, { expiresIn: jwtConfig.expiresIn });
  }
}
