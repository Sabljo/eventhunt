import { UsersService } from './users.service.js';

export class UsersController {
  static async register(req, res) {
    const user = req.body;
    try {
      console.log(user);
      const token = await UsersService.register(user);
      res.send({token});
    } catch (error) {
      res.status(error.code).send({ error: error.message });
    }
  }

  static async login(req, res) {
    const { username, password } = req.body;
    try {
      const token = await UsersService.login(username, password);
      res.json(token );
    } catch (error) {
      res.status(error.code).send({ error: error.message });
    }
  }
}