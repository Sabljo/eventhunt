
import express from 'express';
import bodyParser from 'body-parser';
import { apiRouter } from './api.router.js';
import config from 'config';
import path from 'path';

(async function () {
  const app = express();
  app.use(bodyParser.json()); 
  app.use('/api', apiRouter);
  app.use('/server/src/images', express.static(path.join('src/images')));

  app.listen(config.get('server.port'), () => console.log(`Listening on port ${config.get('server.port')}`));
})();



