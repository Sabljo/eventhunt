import { EventsService } from './events.service.js';

export class EventsController {
  static async create(req, res) {
    const event = req.body;
    const picture = {
      picture:'http://localhost:3000/server/src/images/' + req.file.filename 
    }
    const new_event={...event,...picture}
    await EventsService.create(new_event);
    res.status(201);
    res.send(EventsController._mapToViewModel(new_event));
  }

  static async get(req, res) {
    const events = await EventsService.find();
    res.send(events.map(EventsController._mapToViewModel));
  }

  static async getById(req, res) {
    const event = await EventsService.findById(req.params.id);
    if (!event) {
      res.status(404);
      res.send({ error: 'Not found!' });
      return;
    }
    res.send(EventsController._mapToViewModel(event));
  }

  static async delete(req, res) {
    const deleted = await EventsService.delete(req.params.id);
    if (!deleted) {
      res.status(404);
      res.send({ error: 'Not found!' });
      return;
    }
    res.send();
  }

  static _mapToViewModel(event) {
    event.id =event._id;
    delete event._id;
    return event;
  }
}