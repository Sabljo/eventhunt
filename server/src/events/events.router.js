import { Router } from 'express';
import { jwtMiddleware } from '../auth/jwt.middleware.js';
import { EventsController } from './events.controller.js';
import  {storages} from './storage.js';
export const eventsRouter = Router();
const route = '/';

eventsRouter.post(route,storages, jwtMiddleware, EventsController.create);

eventsRouter.get(route, EventsController.get);

eventsRouter.get(`${route}:id`, EventsController.getById);

eventsRouter.delete(`${route}:id`, jwtMiddleware, EventsController.delete);

 

