import mongodb from 'mongodb';
import { database } from '../database/database.js';
import fs from 'fs';

export class EventsService {
  static _collection = database.collection('events');

  static async create(event) {
    await EventsService._collection.insertOne(event);
  }

  static async find() {
    return await EventsService._collection.find().toArray();
  }

  static async findById(id) {
    var ObjectID = mongodb.ObjectID;
    var objectId = new ObjectID(id);  
    return await EventsService._collection.findOne({_id:objectId});
  }

  static async delete(id) {
     id = mongodb.ObjectId(id);
     const path = 'src/images/'+(await EventsService._collection.findOne({_id:id})).picture.split("/").pop();
     console.log(path);
      fs.unlink(path, (err) => {
        if (err) {
            console.error(err)
        return
      }
    })
    const deletedCount = (await EventsService._collection.deleteOne({ _id: id })).deletedCount;
    if (deletedCount === 0) {
      return false;
    }
    return true;
  }
}
