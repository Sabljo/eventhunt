import { Router } from 'express';
import { eventsRouter } from './events/events.router.js';
import { usersRouter } from './users/users.router.js';

export const apiRouter = Router();

apiRouter.use('/events', eventsRouter);
apiRouter.use('/users', usersRouter);